import cairo
import numpy.random as rd
import numpy as np
import colorsys
import matplotlib.pyplot as plt


# %%
class RandomRingSet():

    def __init__(self, x, y, rmax, relwidth, colors=None):
        width = self.ringWidth(rmax, relwidth)
        n = self.nCircles(rmax, width)
        self.rad = self.randomRadius(rmax, width, n)
        self.stroke = self.randomStroke(width, n)
        self.x, self.y = x, y

    def ringWidth(self, rmax, relwidth):
        return rmax - (1 - relwidth)*rmax

    def randomRadius(self, rmax, width, n):
        return np.sort(rd.randint(rmax-width, rmax, n))

    def randomStroke(self, ringWidth, n):
        smin, smax = np.array([.002, .02])*ringWidth
        return rd.randint(smin, smax, n)

    def nCircles(self, rmax, ringWidth):
        return np.int(rd.randint(90, 240)/rmax*ringWidth)

    @property
    def pos(self):
        return np.array([self.x, self.y])

    @property
    def rmax(self):
        return self.rad[-1]

    @property
    def rmin(self):
        return self.rad[0]


class colors():

    def __init__(self, hue=None):
        if hue:
            self.primary = hue
        else:
            self.primary = self.randomHue()
        self.accent = self.randomAccent()
        self.bg_static = self.bg()

    def randomHue(self):
        return rd.randint(0, 360)

    def randomAccent(self):
        return (self.primary + rd.randint(160, 200)) % 360

    def fgRandomSat(self):
        return .2*rd.random() + .4

    def bgRandomSat(self):
        return .2*rd.random() + .2

    def fgRandomVal(self):
        return .2*rd.random() + .8

    def bgRandomVal(self):
        return .15*rd.random()

    def bg(self, weight=1):
        h = self.primary/360
        s = self.bgRandomSat()
        v = weight*self.bgRandomVal()
        return colorsys.hsv_to_rgb(h, s, v)

    def fgPrimary(self, weight=1):
        h = self.primary/360
        s = self.fgRandomSat()
        v = weight*self.fgRandomVal()
        return colorsys.hsv_to_rgb(h, s, v)

    def fgAccent(self, weight=1):
        h = self.accent/360
        s = self.fgRandomSat()
        v = weight*self.fgRandomVal()
        return colorsys.hsv_to_rgb(h, s, v)


class monochrome():

    def __init__(self):
        self.bg_static = self.bg()

    def fgPrimary(self, weight=1):
        return (.9, .9, .9)

    def fgAccent(self, weight=1):
        return (.9, .9, .9)

    def bg(self, weight=1):
        return (.1, .1, .1)


def selectRandomCorner(w, h):
    corners = [(0,0), (w,0), (w, h), (0, h)]
    return corners[rd.choice(4)]

def firstRingRadius(w, h):
    return np.max([w,h])*(.2*rd.random() + .9)

def ringWidth():
    return .2*rd.random()+.25

def firstRingPos(w, h):
    x, y = selectRandomCorner(w, h)
    x += (.4*rd.random() - .2)*w
    y += (.4*rd.random() - .2)*h
    return x, y

def tangentRingPos(tanring, rmax, w, h):
    tanx, tany = -5*w, -5*h
    r = tanring.rad[0]
    while (tanx > 1.1*w) or (tany > 1.1*h) or (tanx < -.1*w) or (tany < -.1*h):
        phi = 2*np.pi*rd.random()
        tanx = tanring.x + r*np.cos(phi)
        tany = tanring.y + r*np.sin(phi)
    x = tanx - rmax*np.cos(phi)
    y = tany - rmax*np.sin(phi)
    return x, y

def tangentRingRadius(tanring):
    return tanring.rmin*(.3*rd.random() + .5)

def overlapRingPos(ring, w, h):
    x, y = -w, -h
    while (x > w) or (y > h) or (x < 0) or (y < 0):
        r = rd.choice(ring.rad)
        phi = 2*np.pi*rd.random()
        x = ring.x + r*np.cos(phi)
        y = ring.y + r*np.sin(phi)
    return x, y

def overlapRingRadius(ring):
    return ring.rmax*(.3*rd.random() + .2)

def drawBG(w, h, color, ctx):
    ctx.rectangle(0,0,w,h)
    ctx.set_source_rgb(*color.bg_static)
    ctx.fill()

def drawRing(ctx, color, ring):

    ctx.push_group()
    ctx.arc(*ring.pos, ring.rmax, 0, 2*np.pi)
    ctx.set_source_rgb(*color.bg_static)
    ctx.fill()
    ctx.set_operator(cairo.Operator.CLEAR)
    ctx.arc(*ring.pos, ring.rmin, 0, 2*np.pi)
    ctx.fill()
    ctx.pop_group_to_source()
    ctx.paint()

    rgb = [color.fgPrimary, color.fgAccent][rd.choice(2)]
    for r, s in zip(ring.rad, ring.stroke):
        ctx.arc(*ring.pos, r, 0, 2*np.pi)
        ctx.set_source_rgb(*rgb(r/ring.rmax))
        ctx.set_line_width(s)
        ctx.stroke()


def draw(w, h, output, color=monochrome()):

    surf = cairo.ImageSurface(cairo.FORMAT_ARGB32, w, h)
    ctx = cairo.Context(surf)

    drawBG(w, h, color, ctx)

    pos = firstRingPos(w, h)
    rmax = firstRingRadius(w, h)
    relwidth = ringWidth()

    ring = RandomRingSet(*pos, rmax, relwidth)

    drawRing(ctx, color, ring)

    rmax = tangentRingRadius(ring)
    pos = tangentRingPos(ring, rmax, w, h)
    relwidth = ringWidth()

    ring2 = RandomRingSet(*pos, rmax, relwidth)

    drawRing(ctx, color, ring2)

    pos = overlapRingPos(ring, w, h)
    rmax = overlapRingRadius(ring)
    relwidth = ringWidth()

    ring3 = RandomRingSet(*pos, rmax, relwidth)

    drawRing(ctx, color, ring3)
    #ctx.fill()

    surf.write_to_png(output)


# %%
w, h =  2000, 2000
for i in range(1, 21):
    draw(w, h, f"img/02/{w}x{h}_{i:02}.png", colors())


# %%
bla = np.array([[1,2], [3,4], [5,6]])
rd.choice(list(bla))

# %%
len(bla)
